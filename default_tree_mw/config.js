const Config = {
    alerting: {
        fetchBroadcastUrl: "./broadcastedAlerts.json",
        localStorageDisplayedAlertsKey: "alerts"
    },
    wfsImgPath: "./resources/img/",
    ignoredKeys: ["shape"],
    namedProjections: [
        ["EPSG:25832", "+title=ETRS89/UTM 32N +proj=utm +zone=32 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs"],
        ["EPSG:4326", "+title=WGS 84 (long/lat) +proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs"]
    ],
    tree: {
        orderBy: "Inspire", //"Opendata", "Inspire", "Behörde"
        layerIDsToIgnore: [],
        metaIDsToIgnore: []
    },
    footer: {
        urls: [
            {
                "bezeichnung": "Kartographie und Gestaltung: ",
                "url": "https://www.muenchen.de/rathaus/Stadtverwaltung/Kommunalreferat/geodatenservice.html",
                "alias": "Kommunalreferat GeodatenService München",
                "alias_mobil": "KOM GSM"
            },
            {
                "bezeichnung": "",
                "url": "https://stadt.muenchen.de/infos/impressum-datenschutz.html",
                "alias": "Impressum",
                "alias_mobil": "Impressum"
            }
        ],
        
        showVersion: true
    },
    layerConf: "./resources/services.json",
    restConf: "./resources/rest-services.json",
    styleConf: "./resources/style.json",
    scaleLine: true,
    mapMarker: {
        pointStyleId: "customMapMarkerPoint"
    },
    portalLanguage: {
        enabled: true,
        debug: false,
        languages: {
            de: "deutsch",
            en: "englisch"
        },
        fallbackLanguage: "de",
        changeLanguageOnStartWhen: ["querystring", "localStorage", "navigator", "htmlTag"]
    }
};

// conditional export to make config readable by e2e tests
if (typeof module !== "undefined") {
    module.exports = Config;
}
