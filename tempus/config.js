const Config = {
    alerting: {
        fetchBroadcastUrl: "./broadcastedAlerts.json",
        localStorageDisplayedAlertsKey: "alerts"
    },
    wfsImgPath: "./resources/img/",
    ignoredKeys: ["shape", "geometry"],
    namedProjections: [
        ["EPSG:25832", "+title=ETRS89/UTM 32N +proj=utm +zone=32 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs"],
        ["EPSG:4326", "+title=WGS 84 (long/lat) +proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs"]
    ],
    footer: {
        urls: [
            {
                "bezeichnung": "powered by: ",
                "url": "https://www.muenchen.de/rathaus/Stadtverwaltung/Kommunalreferat/geodatenservice.html",
                "alias": "GeodatenService München",
                "alias_mobil": "GSM"
            }
        ],
        showVersion: true
    },
    mapInteractions: {
        interactionModes: {
            altShiftDragRotate: true,
            pinchRotate: true
        }
    },
    layerConf: "./resources/services.json",
    restConf: "./resources/rest-services.json",
    styleConf: "./resources/style.json",
    scaleLine: true,
    mouseHover: {
        numFeaturesToShow: 2,
        infoText: "(weitere Objekte. Bitte zoomen.)",
        highlightOnHover: true,
        highlightVectorRulesPolygon: {
            fill: {
                color: [255, 255, 255, 0.5]
            },
            stroke: {
                width: 4,
                color: [255, 0, 0, 0.9]
            }
        },
        highlightVectorRulesPointLine: {
            stroke: {
                width: 8,
                color: [255, 0, 255, 0.9]
            },
            image: {
                scale: 1.5
            }
        }
    },
    mapMarker: {
        pointStyleId: "customMapMarkerPoint"
    }
};

// conditional export to make config readable by e2e tests
if (typeof module !== "undefined") {
    module.exports = Config;
}
