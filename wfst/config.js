const Config = {
    alerting: {
        fetchBroadcastUrl: "./broadcastedAlerts.json",
        localStorageDisplayedAlertsKey: "alerts"
    },
    wfsImgPath: "./resources/img/",
    ignoredKeys: ["shape"],
    namedProjections: [
        ["EPSG:25832", "+title=ETRS89/UTM 32N +proj=utm +zone=32 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs"],
        ["EPSG:4326", "+title=WGS 84 (long/lat) +proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs"]
    ],
    footer: {
        urls: [
            {
                "bezeichnung": "Kartographie und Gestaltung: ",
                "url": "https://www.muenchen.de/rathaus/Stadtverwaltung/Kommunalreferat/geodatenservice.html",
                "alias": "Kommunalreferat GeodatenService München",
                "alias_mobil": "KOM GSM"
            }
        ],
        footerInfo: [
            {
                title: "Contact",
                description: "Text under the titel",
                subtexts: [
                    {
                        subtitle: "Postal address",
                        text: "Max-Mustermann-Str. 1 <br> 12345 City <br> Germany"
                    },
                    {
                        subtitle: "Phone and fax",
                        text: "Tel: +49 (0) 1234 56789 <br> Fax: +49 (0) 1234 5678910"
                    }
                ]
            },
            {
                title: "Privacy",
                subtexts: [
                    {
                        subtitle: "Subtitle",
                        text: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt..."
                    }
                ]
            }
        ],
        showVersion: true
    },
    layerConf: "./resources/services.json",
    restConf: "./resources/rest-services.json",
    styleConf: "./resources/style.json",
    scaleLine: true,
    mouseHover: {
        numFeaturesToShow: 2,
        infoText: "(weitere Objekte. Bitte zoomen.)",
        highlightOnHover: true,
        highlightVectorRulesPolygon: {
            fill: {
                color: [255, 255, 255, 0.5]
            },
            stroke: {
                width: 4,
                color: [0, 255, 255, 0.9]
            }
        },
        highlightVectorRulesPointLine: {
            stroke: {
                width: 8,
                color: [0, 255, 255, 0.9]
            },
            image: {
                scale: 1.5
            }
        }
    },
    mapMarker: {
        pointStyleId: "customMapMarkerPoint"
    },
    portalLanguage: {
        enabled: true,
        debug: false,
        languages: {
            de: "deutsch",
            en: "englisch",
            it: "italienisch",
            pt: "portugiesisch",
            es: "spanisch"
        },
        fallbackLanguage: "de",
        changeLanguageOnStartWhen: ["querystring", "localStorage", "navigator", "htmlTag"]
    }
};

// conditional export to make config readable by e2e tests
if (typeof module !== "undefined") {
    module.exports = Config;
}
